import React from "react";
import { Link } from "react-router-dom";
//MUI
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { makeStyles } from "@material-ui/core/styles";
//Icons
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import HomeIcon from "@material-ui/icons/Home";
import PeopleIcon from "@material-ui/icons/People";

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  toolbar: {
    minHeight: 106,
  },
  link: {
    textDecoration: "none",
    color: theme.palette.text.primary,
  },
 
}));

function DrawerLink() {
  const classes = useStyles();

  return (
    <div>
      <div className={classes.toolbar} />
      <List>
        <Link to="/home" className="nav-text">
          <ListItem>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary={"Home"} />
          </ListItem>
        </Link>
        <Link to="/registeruser" className="nav-text">
          <ListItem>
            <ListItemIcon>
              <PersonAddIcon />
            </ListItemIcon>
            <ListItemText primary={"Register"} />
          </ListItem>
        </Link>
        <Link to="/users" className="nav-text">
          <ListItem>
            <ListItemIcon>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText primary={"Users"} />
          </ListItem>
        </Link>
      </List>
    </div>
  );
}

export default DrawerLink;
