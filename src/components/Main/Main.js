import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
//MUI
import { makeStyles } from "@material-ui/core/styles";
//Pages
import Home from "../Pages/Home";
import Signup from "../Pages/Signup";
import Users from "../Pages/Users";
import Sidebar from "../Sidebar/Sidebar";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: theme.mixins.toolbar,
  toolbar: {
    minHeight: 106,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function Main() {
  const classes = useStyles();

  return (
    <Router>
      <div className={classes.root}>
        <Sidebar />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Switch>
            <Route exact path="/home" component={Home}></Route>
            <Route exact path="/registeruser" component={Signup}></Route>
            <Route exact path="/users" component={Users}></Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default Main;
