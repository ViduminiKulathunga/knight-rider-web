import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

//import routers
import MainRoutes from "./components/Main/Main";
import Login from "./components/Login/Login";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#1e88e5",
      main: "#1e88e5",
      dark: "#008394",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff6333",
      main: "#d81b60",
      dark: "#b22a00",
      contrastText: "#fff",
    },
  }
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <Router>
            <Switch>
              <Route exact path="/" component={MainRoutes} />
              <Route exact path="/login" component={Login} />
            </Switch>
          </Router>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
